# myowncloud

## Ejecución
```
$ docker compose up -d
```
Para acceder agregar en `/etc/hosts`:
```
127.0.0.1 owncloud.local
```
## Configuración inicial:
- Usuario y contraseña de owncloud: indicar valores deseados
- Usuario BD: **root**
- Contraseña BD: generada aleatoriamente al inicio de la ejecución (ver apartado siguiente)
- Host BD: **mariadb**
- Directorio de datos: dejar el que ofrece por defecto (**/var/www/owncloud/data**)
- Base de datos: especificar un nombre para la base de datos
### Contraseña BD (root)
Para obtener la contraseña de root generada aleatoriamente, acceder al log del contenedor de mariadb en ejecución:
```
$ docker logs myowncloud-mariadb-1 | less
```
